<?php

namespace MadBob\EasyRDFonGuzzle;

use EasyRdf\Http\Client as NativeClient;
use EasyRdf\Http\Response;
use GuzzleHttp\Client;

class HttpClient extends NativeClient
{
    private $client;
    private $redirectCounter = 0;
    private $config = [
        'maxredirects'    => 5,
        'useragent'       => 'EasyRdf-on-Guzzle HTTP Client',
        'timeout'         => 10
    ];
    private $headers = [];
    private $options = [];

    public function __construct($uri = null, $config = null)
    {
        $this->client = new Client();

        if ($uri !== null) {
            $this->setUri($uri);
        }

        if ($config !== null) {
            $this->setConfig($config);
        }
    }

    public function setConfig($config = array())
    {
        if ($config == null or !is_array($config)) {
            throw new \InvalidArgumentException("\$config should be an array and cannot be null");
        }

        foreach ($config as $k => $v) {
            $this->config[strtolower($k)] = $v;
        }

        return $this;
    }

    public function setHeaders($name, $value = null)
    {
        $normalizedName = strtolower($name);

        if ($value === null || $value === false) {
            unset($this->headers[$normalizedName]);
        }
        else {
            $this->headers[$normalizedName] = array($name, $value);
        }

        return $this;
    }

    public function setOptions($name, $value = null)
    {
        $normalizedName = strtolower($name);

        if ($value === null || $value === false) {
            unset($this->options[$normalizedName]);
        }
        else {
            $this->options[$normalizedName] = array($name, $value);
        }

        return $this;
    }

    public function getHeader($key)
    {
        $key = strtolower($key);
        if (isset($this->headers[$key])) {
            return $this->headers[$key][1];
        }
        else {
            return null;
        }
    }

    public function getRedirectionsCount()
    {
        return $this->redirectCounter;
    }

    public function resetParameters($clearAll = false)
    {
        parent::resetParameters($clearAll);

        if ($clearAll) {
            $this->headers = [];
        }
        else {
            if (isset($this->headers['content-type'])) {
                unset($this->headers['content-type']);
            }
            if (isset($this->headers['content-length'])) {
                unset($this->headers['content-length']);
            }
        }

        return $this;
    }

    public function request($method = null)
    {
        if (!$this->getUri()) {
            throw new Exception(
                "Set URI before calling Client->request()"
            );
        }

        if ($method) {
            $this->setMethod($method);
        }

        $options = $this->doOptions();

        try {
            $response = $this->client->request($this->getMethod(), $this->getUri(), $options);
            $this->redirectCounter = count(explode(',', $response->getHeaderLine('X-Guzzle-Redirect-Status-History')));
        }
        catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
        }

        $response = Response::fromString($this->rawResponse($response));

        return $response;
    }

    protected function doOptions()
    {
        $options = [];

        $params = $this->getParametersGet();
        if (!empty($params) && !isset($this->options['query'])) {
            $options['query'] = $params;
        }

        $body = $this->getRawData();
        if (!empty($body) && !isset($this->options['body'])) {
            $options['body'] = $body;
        }

        if (!isset($this->options['headers'])) {
            $options['headers'] = $this->doHeaders();
        }

        if (!isset($this->options['timeout'])) {
            $options['timeout'] = $this->config['timeout'];
        }

        if (!isset($this->options['allow_redirects'])) {
            $options['allow_redirects'] = [
                'max' => $this->config['maxredirects'],
                'strict' => true,
                'track_redirects' => true,
             ];
        }

        if (!isset($this->headers['connection'])) {
            $headers['Connection'] = 'close';
        }

        foreach ($this->options as $opt) {
            list($name, $value) = $opt;
            $options[$name] = $value;
        }

        return $options;
    }

    protected function doHeaders()
    {
        $headers = [];

        if (!isset($this->headers['user-agent'])) {
            $headers['User-Agent'] = $this->config['useragent'];
        }

        if (!isset($this->headers['connection'])) {
            $headers['Connection'] = 'close';
        }

        foreach ($this->headers as $header) {
            list($name, $value) = $header;
            if (is_array($value)) {
                $value = implode(', ', $value);
            }

            $headers[$name] = $value;
        }

        return $headers;
    }

    protected function rawResponse($message)
    {
        $msg = 'HTTP/' . $message->getProtocolVersion() . ' ' . $message->getStatusCode() . ' ' . $message->getReasonPhrase();

        foreach ($message->getHeaders() as $name => $values) {
            /*
                Guzzle already decodes chunked contents, and when EasyRDF finds
                this header tries to re-decode it, failing
            */
            if (strtolower($name) == 'transfer-encoding') {
                continue;
            }

            $msg .= "\r\n{$name}: " . implode(', ', $values);
        }

        return "{$msg}\r\n\r\n" . $message->getBody();
    }

    public function __call($method, $parameters)
    {
        return $this->client->$method(...$parameters);
    }
}
