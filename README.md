EasyRDF on Guzzle
=================

The missing link between EasyRDF and Guzzle!

With this connector you can use Guzzle for your EasyRDF's SPARQL client, and have greater and easier control over your HTTP connections.

Install it with composer:

```
composer require madbob/easyrdf-on-guzzle
```

And wire your new `HttpClient` with EasyRDF:

```
use MadBob\EasyRDFonGuzzle\HttpClient;

$httpclient = new HttpClient();
\EasyRdf\Http::setDefaultHttpClient($httpclient);
```

`setHeaders()` and `setParameterGet()` functions are the same of native `EasyRdf\Http\Client` class, and using the new `setOptions()` function you can fine tune the client with the [many options provided by Guzzle](http://docs.guzzlephp.org/en/stable/request-options.html).

Pay attention: the internal Guzzle client is configured with defaults, and not all functions are enabled with this setup. For example, to actually use the `auth` parameter you have to enforce the cURL handler as follows:

```
use MadBob\EasyRDFonGuzzle\HttpClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\CurlHandler;

$httpclient = new HttpClient();

$handler = new CurlHandler();
$stack = HandlerStack::create($handler);
$httpclient->setOptions('handler', $stack);

$httpclient->setOptions('auth', [$username, $password, 'digest']);

\EasyRdf\Http::setDefaultHttpClient($httpclient);

```
